pipelineJob('pipeline-job-dsl-example') {
    definition {
        cpsScm {
            scm {
                git {
                	remote {
                		name('DSL-repo')
                		url('https://bitbucket.org/mykhalchuk_oleksandr/jenkins-jobs.git')
            		}
            		branches('*/master')
                }
            }
            scriptPath('pipelines/test-pipeline/Jenkinsfile')
        }
    }
}